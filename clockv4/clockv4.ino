/** pre-refactor: one big file */

#include <EEPROM.h>
#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
#include <avr/power.h> // Required for 16 MHz Adafruit Trinket
#endif

#include "RTClib.h"

RTC_DS3231 rtc;



/**********************************************
   pound defined constants
  VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV*/
// set lower to increase speed: 1 = 1ms per second
// set higher to emulate fullspeed: 1000 = 1000ms per second
#define SPEED 1000
#define DEBUG true
#define STARTING_BRIGHTNESS 40

// aspects of the display, can't simply change these:
#define NUM_ROWS 12
#define NUM_COLS 12
#define NUM_BUTTONS 3
/*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/


/**********************************************
   display state: what pixels are on?
  VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV*/
uint32_t pixels[NUM_ROWS][NUM_COLS];
/*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/

uint32_t MAX = 4294967295;

/**********************************************
   Time, brightness state
  VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV*/
// milli count on last loop
uint32_t lastMillis = 0;

// current time split out into demoninations:
uint16_t ms = 0;
uint8_t second = 0;
uint8_t minute = 0;
uint8_t hour = 0;
uint8_t day = 5;
uint8_t month = 1;
uint8_t year = 21;

uint8_t DAY_ADDR = 0;
uint8_t MONTH_ADDR = DAY_ADDR + sizeof(uint8_t);
uint8_t YEAR_ADDR = MONTH_ADDR + sizeof(uint8_t);

uint32_t bullshitOffset = 4294960000;
//uint32_t todaySoFar = 86391001;
uint32_t todaySoFar = 0;

int brightness = STARTING_BRIGHTNESS;

bool firstSecond = false;
/*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/


/**********************************************
   LED Index by location
  VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV*/
// writing out the matrix puts y first, as in _INDEX_MAP[y][x]
// so during startup, reflect it over y=x to format it into INDEX_MAP[x][y]
int INDEX_MAP[12][12];

int _INDEX_MAP[12][12] = {
  {   0,  23,  24,  47,  48,  71,  72,  95,  96, 119, 120, 143 },
  {   1,  22,  25,  46,  49,  70,  73,  94,  97, 118, 121, 142 },
  {   2,  21,  26,  45,  50,  69,  74,  93,  98, 117, 122, 141 },
  {   3,  20,  27,  44,  51,  68,  75,  92,  99, 116, 123, 140 },
  {   4,  19,  28,  43,  52,  67,  76,  91, 100, 115, 124, 139 },
  {   5,  18,  29,  42,  53,  66,  77,  90, 101, 114, 125, 138 },
  {   6,  17,  30,  41,  54,  65,  78,  89, 102, 113, 126, 137 },
  {   7,  16,  31,  40,  55,  64,  79,  88, 103, 112, 127, 136 },
  {   8,  15,  32,  39,  56,  63,  80,  87, 104, 111, 128, 135 },
  {   9,  14,  33,  38,  57,  62,  81,  86, 105, 110, 129, 134 },
  {  10,  13,  34,  37,  58,  61,  82,  85, 106, 109, 130, 133 },
  {  11,  12,  35,  36,  59,  60,  83,  84, 107, 108, 131, 132 },
};
/*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/


/**********************************************
   Button state
  VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV*/
int BUTTON_MODES = 6;
uint32_t lastInteraction = -1;
int BUTTON_MODE = -1;
bool buttonsPressed[NUM_BUTTONS] = { false, false, false };
int buttons[NUM_BUTTONS] = { A0, A1, A2 };
/*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/




// Which pin on the Arduino is connected to the NeoPixels?
#define PIN        4 // On Trinket or Gemma, suggest changing this to 1

// How many NeoPixels are attached to the Arduino?
#define NUMPIXELS 144 // Popular NeoPixel ring size

// When setting up the NeoPixel library, we tell it how many pixels,
// and which pin to use to send signals. Note that for older NeoPixel
// strips you might need to change the third parameter -- see the
// strandTest example for more information on possible values.
Adafruit_NeoPixel strip(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

/**********************************************
   Colors
  VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV*/
uint32_t WHITE = strip.Color(255, 255, 255);
uint32_t GREEN = strip.Color(255,   0,   0);
uint32_t RED   = strip.Color(  0, 255,   0);
uint32_t BLUE  = strip.Color(  0,   0, 255);
uint32_t PURP  = strip.Color(  0, 255, 255);
/*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/

// ??
uint32_t todayMillis = 0;
char buf[100];


/**********************************************
   Test Sequence
  VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV*/
bool testDone = true;
bool tSeq = 0;
/*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/

/**********************************************
   setup() and loop()
  VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV*/
void setup() {
#if defined(__AVR_ATtiny85__) && (F_CPU == 16000000)
  clock_prescale_set(clock_div_1);
#endif
  if (DEBUG) {
    Serial.begin(38400);
    Serial.println("setup");
#ifndef ESP8266
    while (!Serial); // wait for serial port to connect. Needed for native USB
#endif
  }

  if (! rtc.begin()) {
    if (DEBUG) {
      Serial.println("Couldn't find RTC");
      Serial.flush();  
    }
    abort();
  }

  if (rtc.lostPower()) {
    if (DEBUG) {
      Serial.println("RTC lost power, let's set the time!");
      Serial.flush();  
    }
    // When time needs to be set on a new device, or after a power loss, the
    // following line sets the RTC to the date & time this sketch was compiled
//    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
    // This line sets the RTC with an explicit date & time, for example to set
    // January 21, 2014 at 3am you would call:
    
  }
  rtc.adjust(DateTime(year, month, day, hour, minute, second));
  
  reflectMap();

  off();

  strip.begin(); // INITIALIZE NeoPixel strip object (REQUIRED)
  strip.show();
  strip.setBrightness(brightness);

  lastMillis = millis();
  readAll();
}

void readAll() {
  if (DEBUG) {
    Serial.print("READALL BEFORE:: ");
    printTime();
  }
  day = EEPROM.read(DAY_ADDR);
  month = EEPROM.read(MONTH_ADDR);
  year = EEPROM.read(YEAR_ADDR);
}

// TODO lower
void reflectMap() {
  // reflect over y = -x
  for (int x = 0; x < NUM_COLS; x++) {
    for (int y = 0; y < NUM_ROWS; y++) {
      INDEX_MAP[x][y] = _INDEX_MAP[NUM_ROWS - 1 - y][x];
    }
  }
}

/**
   Called by the controller in a loop repeatedly.
*/

boolean uDone = false;
void loop() {
  boolean rerender = updateClock();
  readButtonStates();
  checkButtonIdle();

    off();
  if (!testDone) {
    testSequence();
  } else if (!uDone) {
    rerender = upSequence();
//    uDone = true;
  } else if (inButtonMode()) {
    renderButtonMode();
  } else if (inBirthdayMode() && second % 10 <= 3) {
    renderBirthday();
  } else {
    renderClock();
  }

//  if (rerender) {
  draw();
//  }
}

uint32_t lastSecond = 0;

bool updateClock() {
  uint32_t thisMillis = millis();

  uint32_t diff = thisMillis - lastMillis;
  if (diff >= 100) {
    lastMillis = thisMillis;
    todaySoFar += diff;
    uupp();
    return true;
  }

  return false;
}

// TODO put these at the top
unsigned int MS_PER_SECOND = 1000;
unsigned int SECONDS_PER_MINUTE = 60;
unsigned int MINUTES_PER_HOUR = 60;
unsigned int HOURS_PER_DAY = 24;

uint32_t MS_PER_MINUTE = MS_PER_SECOND * SECONDS_PER_MINUTE; // 60,000
uint32_t MS_PER_HOUR = MS_PER_MINUTE * MINUTES_PER_HOUR; //  3,600,000
uint32_t MS_PER_DAY = MS_PER_HOUR * HOURS_PER_DAY; // 86,400,000

void uupp() {
  int _second = (todaySoFar / MS_PER_SECOND) % SECONDS_PER_MINUTE;
  if (second != _second) {
    //if (!firstSecond) {
    //  Serial.println("da fuck.");
    //  firstSecond = true;
    //  readAll();
    //}
    
    second = _second;
    setTime();
    printTime();
    DateTime now = rtc.now();
    Serial.println(now.second(), DEC);
  }

  if (todaySoFar > MS_PER_DAY) {
    dayTick();
  }
}

void setTime() {
  minute = (todaySoFar / MS_PER_MINUTE) % MINUTES_PER_HOUR;
  hour = (todaySoFar / MS_PER_HOUR) % HOURS_PER_DAY;
}


void printTime() {
  sprintf(buf, "todaySoFar = %ld : 2000+%d %d/%d %d:%d:%d", todaySoFar, year, month, day, hour, minute, second);
  Serial.println(buf);
}

/*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/


/**********************************************
   Display utilities
  VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV*/
/** display the pixels that are on */
void draw() {
  for (int y = 0; y < NUM_ROWS; y++) {
    for (int x = 0; x < NUM_COLS; x++) {
      uint32_t color = pixels[x][y];
      if (color != MAX) {
        int index = INDEX_MAP[x][y];
        strip.setPixelColor(index, color);
        //        if (second % 2 == 0) {
        //          strip.setPixelColor(index, WHITE);
        //        } else {
        //          strip.setPixelColor(index, GREEN);
        //        }
      }
    }
  }
  strip.show();
}

/** turn off all the pixels */
void off() {
  strip.clear(); // Set all pixel colors to 'off'
  for (int x = 0; x < NUM_COLS; x++) {
    for (int y = 0; y < NUM_ROWS; y++) {
      pixels[x][y] = MAX;
    }
  }
}

/** turn on all of the lights: */
void all() {
  for (int x = 0; x < NUM_COLS; x++) {
    for (int y = 0; y < NUM_ROWS; y++) {
      pixels[x][y] = WHITE;
    }
  }
}

/** turn on all of the lights: */
void colorAll(uint32_t color) {
  for (int x = 0; x < NUM_COLS; x++) {
    for (int y = 0; y < NUM_ROWS; y++) {
      pixels[x][y] = color;
    }
  }
}


int strandTestx = 0;
int strandTesty = 0;
int strandTestStep = 0;
int strandTestMode = 0;
int COUNT_STRAND_TEST_MODES = 2;
int stepDelay = 100;
unsigned long strandTestLast = 0;


int stepCounts[2] = {
  NUM_ROWS * NUM_COLS,
  NUM_ROWS * NUM_COLS,
};

unsigned long lastRender = 0;
bool testSequence() {
  // if we haven't initialized this yet, do so and skip a frame
  if (strandTestLast == 0) {
    strandTestLast = lastMillis;
    return;
  }
  switch (tSeq) {
    case 0:
      tSeq += tSeq0Render ? 1 : 0;
      break;
    case 1:
      tSeq += tSeq1Render ? 1 : 0;
      break;
    default:
      testDone = true;
  }
  return true;
}

bool tSeq0Render() {
  // Fill along the length of the strip in various colors...
  colorWipe(strip.Color(255,   0,   0), 50); // Red
  colorWipe(strip.Color(  0, 255,   0), 50); // Green
  colorWipe(strip.Color(  0,   0, 255), 50); // Blue

  // Do a theater marquee effect in various colors...
  theaterChase(strip.Color(127, 127, 127), 50); // White, half brightness
  theaterChase(strip.Color(127,   0,   0), 50); // Red, half brightness
  theaterChase(strip.Color(  0,   0, 127), 50); // Blue, half brightness

  rainbow(10);             // Flowing rainbow cycle along the whole strip
  theaterChaseRainbow(50); // Rainbow-enhanced theaterChase variant
}

bool tSeq3Render() {
  //  if (lastMillis - lastRender > 10) {
  //    lastRender = lastMillis;
  //    strandTestx++;
  //    if (strandTestx >= NUM_COLS) {
  //      strandTestx = 0;
  //      strandTesty = (strandTesty + 1) % NUM_ROWS;
  //    }
  //    off();
  //    pixels[strandTestx][strandTesty] = WHITE;
  //    strandTestStep = (strandTestStep + 1) % stepCounts[0];
  //    return strandTestStep == 0;
  //  }
  colorAll(WHITE);
  return lastMillis - lastRender > 1000;
}

bool tSeq1Render() {
  colorAll(GREEN);
  return lastMillis - lastRender > 1000;

  //  if (lastMillis - lastRender > 10) {
  //    lastRender = lastMillis;
  //    strandTestx++;
  //    if (strandTestx >= NUM_COLS) {
  //      strandTestx = 0;
  //      strandTesty = (strandTesty + 1) % NUM_ROWS;
  //    }
  //    off();
  //    pixels[strandTestx][strandTesty] = GREEN;
  //    strandTestStep = (strandTestStep + 1) % stepCounts[0];
  //    return strandTestStep == 0;
  //  }
}

int STARTUP_PHASES = 4;
int uSeq = 0;

bool upSequence() {
  unsigned long diff = millis() - lastRender;
  switch (uSeq) {
    case 0: uSeq += suSeq0(diff) ? 1 : 0; break;
    case 1: uSeq += suSeq1(diff) ? 1 : 0; break;
    default:
      uDone = true;
  }
  return true;
}


int suSeq0X = 0;
int suSeq0Y = 0;

int shadowX = 0;
int shadowY = 0;

boolean xHeading = true;

int suSeq0XDirection = 1;
int suSeq0YDirection = 1;

int horizSteps = NUM_COLS - 1;
int vertSteps = NUM_ROWS - 1;

int SU_SEQ_0_FS = 16;

int suSeq0Step = 1;
int suSeq0Total = 0;
int SU_SEQ_0_TOTAL = 144;
boolean suSeq0(unsigned long diff) {
  if (diff > SU_SEQ_0_FS) {
    suSeq0Total++;
    off();
    if (horizSteps == 0 && vertSteps == 0) {
      brightness = STARTING_BRIGHTNESS;
      strip.setBrightness(brightness);
      return true;
    }

    lastRender += SU_SEQ_0_FS;

    shadowX = suSeq0X;
    shadowY = suSeq0Y;

    if (xHeading) {
      suSeq0X += suSeq0XDirection;
      if (suSeq0Step == horizSteps) {
        xHeading = false;
        suSeq0XDirection *= -1;
        suSeq0Step = 0;

        // awkward 1-off for the first location (0,0)
        if (suSeq0Y != 0) horizSteps--;
      }
    } else {
      suSeq0Y += suSeq0YDirection;
      if (suSeq0Step == vertSteps) {
        xHeading = true;
        suSeq0YDirection *= -1;
        vertSteps--;
        suSeq0Step = 0;
        float fraction = 1.0 * suSeq0Total / SU_SEQ_0_TOTAL;
        brightness = STARTING_BRIGHTNESS - fraction * STARTING_BRIGHTNESS;
        strip.setBrightness(brightness);
      }
    }
    suSeq0Step++;
  }

  pixels[shadowX][shadowY] = RED;
  pixels[suSeq0X][suSeq0Y] = WHITE;
  pixels[NUM_COLS - 1 - shadowX][NUM_ROWS - 1 - shadowY] = RED;
  pixels[NUM_COLS - 1 - suSeq0X][NUM_ROWS - 1 - suSeq0Y] = WHITE;
  return false;
}

int SU_SEQ_1_FS = 9000;

bool suSeq1Started = false;
boolean suSeq1(unsigned long diff) {
  if (!suSeq1Started) {
    suSeq1Started = true;
    happy(WHITE);
    birthday(WHITE);
    laurie(RED);
    return false;
  }
  if (diff > SU_SEQ_1_FS) {
    off();
    lastRender += SU_SEQ_1_FS;
    return true;
  }
  return false;
}
/*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/

void setBrightness() {
  //  brightness -= 10;
  //  if (brightness < 0) {
  //    brightness = STARTING_BRIGHTNESS;
  //  }
  //  strip.setBrightness(brightness);
  //  strip.show();

}

/**********************************************
   Time math and mutators
  VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV*/

/*
 * PROLLY CAN DELETE THESE vvv
 */
 void secondTick() {
  Serial.println("Second tick");
  second++;
  if (second > 59) {
    minuteTick();
    second = 0;
  }
}
void minuteTick() {
  minute++;
  if (minute > 59) {
    hourTick();
    minute = 0;
  }
}
void hourTick() {
  minute = 0;
  hour++;
  if (hour > 23) {
    dayTick();
    hour = 0;
  }
}
/*
 * PROLLY CAN DELETE THESE ^^^
 */


void advanceMinute(int delta) {   
  todaySoFar = (MS_PER_DAY + todaySoFar + MS_PER_MINUTE * delta) % MS_PER_DAY;
}
void advanceHour(int delta) {
  todaySoFar = (MS_PER_DAY + todaySoFar + MS_PER_HOUR * delta) % MS_PER_DAY;  
}

void dayTick() {
  if (DEBUG) {
    Serial.print("DAY TICK:: ");
    printTime();
  }
  todaySoFar = 0;

  day++;
  if (day > daysInMonth(month, year)) {
    monthTick();
    day = 1;
  }
  EEPROM.put(DAY_ADDR, day);
}
void monthTick() {
  month++;
  if (month > 12) {
    month = 1;
    year++;
    EEPROM.put(YEAR_ADDR, year);
  }
  EEPROM.put(MONTH_ADDR, month);
}

void persistAll() {
  if (DEBUG) {
    Serial.print("PERSIST ");
    printTime();
  }
  EEPROM.put(DAY_ADDR, day);
  EEPROM.put(MONTH_ADDR, month);
  EEPROM.put(YEAR_ADDR, year);
}

/** return the number of days in the given month of the given year */
int dim[13] = {
  -1, // unused
  31, // jan
  28, // feb
  31, // mar
  30, // apr
  31, // may
  30, // jun
  31, // jul
  31, // aug
  30, // sept
  31, // oct
  30, // nov
  31  // dec
};
int daysInMonth(int month, int year) {
  if (month == 2 &&
      year % 4 == 0 &&
      (year % 100 != 0 || year % 400 == 0)) {
    return dim[month] + 1;
  }
  return dim[month];
}
/*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/


/**********************************************
   Clock display:
  VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV*/
/** Displays the clock mode: */
void renderClock() {
  off();
  itIs();
  renderTime();
}

/** figure out what hours/minute words to display and display them */
void renderTime() {
  int displayMinute = minute;
  int displayHour = hour;

  // if we're not on a five-minute mark:
  if (minute % 5 != 0) {
    around();
  }

  // First, determine whether we are past halfway and need to display "til"
  // and advance the display hour and subtract out display minutes:

  // if we've passed the halfway mark (32 minutes past is still "around half"):
  if (minute > 32) {
    displayHour = (displayHour + 1) % 24;
    displayMinute = 60 - minute;

    // display til if we aren't real close to the next hour:
    if (minute < 58) {
      til();
    }
  }
  // otherwise, the normal "past" case, if we aren't "around" the hour
  else if (minute > 2) {
    past();
  }
  
  // turn the right pixels on
  renderMinute(displayMinute);
  renderHour(displayHour);
}

/** Display the minute (er, all the words related to the minute): */
void renderMinute(int displayMinute) {
  if (displayMinute <= 2); // noop case to avoid the others 
  else if (displayMinute <= 7) {
    minuteFive();
    minutes();
  } else if (displayMinute <= 12) {
    minuteTen();
    minutes();
  } else if (displayMinute <= 17) {
    minuteQuarter();
  } else if (displayMinute <= 22) {
    minuteTwenty();
    minutes();
  } else if (displayMinute <= 27) {
    minuteTwenty();
    minuteFive();
    minutes();
  } else if (displayMinute <= 32) {
    minuteHalf();
  }
}

/** Display the hour for the hour stored in state: */
void renderHour(int displayHour) {
  // Now we know which hour (0-23) we're gonna show.
  // Hours 0-11 are am, 12-23 are pm:
  if (displayHour < 12) {
    am();
  } else {
    pm();
  }
  // figure out which hour number to display:
  displayHour %= 12;


  switch (displayHour) {
    case 0: hourTwelve(); break;
    case 1: hourOne(); break;
    case 2: hourTwo(); break;
    case 3: hourThree(); break;
    case 4: hourFour(); break;
    case 5: hourFive(); break;
    case 6: hourSix(); break;
    case 7: hourSeven(); break;
    case 8: hourEight(); break;
    case 9: hourNine(); break;
    case 10: hourTen(); break;
    case 11: hourEleven(); break;
  }
}

/** Minute words: */
void itIs() {
  pixels[0][11] = WHITE;
  pixels[1][11] = WHITE;
  pixels[3][11] = WHITE;
  pixels[4][11] = WHITE;
}
void around() {
  for (int x = 6; x <= 11; x++) pixels[x][11] = WHITE;
}
void minuteFive() {
  for (int x = 7; x <= 10; x++) pixels[x][10] = WHITE;
}
void minuteTen() {
  for (int x = 9; x <= 11; x++) pixels[x][9] = WHITE;
}
void minuteQuarter() {
  pixels[0][8] = WHITE;
  for (int x = 2; x <= 8; x++) pixels[x][8] = WHITE;
}
void minuteTwenty() {
  for (int x = 0; x <= 5; x++) pixels[x][10] = WHITE;
}
void minuteHalf() {
  for (int x = 0; x <= 3; x++) pixels[x][9] = WHITE;
}
void minutes() {
  for (int x = 3; x <= 9; x++) pixels[x][7] = WHITE;
}
void past() {
  for (int x = 0; x <= 3; x++) pixels[x][6] = WHITE;
}
void til() {
  for (int x = 5; x <= 7; x++) pixels[x][6] = WHITE;
}

/** Hour words: */
void hourOne() {
  for (int x = 0; x <= 2; x++) pixels[x][0] = WHITE;
}
void hourTwo() {
  for (int x = 1; x <= 3; x++) pixels[x][4] = WHITE;
}
void hourThree() {
  for (int x = 4; x <= 8; x++) pixels[x][5] = WHITE;
}
void hourFour() {
  for (int x = 0; x <= 3; x++) pixels[x][2] = WHITE;
}
void hourFive() {
  for (int x = 3; x <= 6; x++) pixels[x][3] = WHITE;
}
void hourSix() {
  for (int x = 9; x <= 11; x++) pixels[x][5] = WHITE;
}
void hourSeven() {
  for (int x = 7; x <= 11; x++) pixels[x][4] = WHITE;
}
void hourEight() {
  for (int x = 6; x <= 10; x++) pixels[x][3] = WHITE;
}
void hourNine() {
  for (int x = 8; x <= 11; x++) pixels[x][1] = WHITE;
}
void hourTen() {
  for (int x = 9; x <= 11; x++) pixels[x][6] = WHITE;
}
void hourEleven() {
  for (int x = 0; x <= 5; x++) pixels[x][1] = WHITE;
}
void hourTwelve() {
  for (int x = 6; x <= 11; x++) pixels[x][2] = WHITE;
}
void am() {
  pixels[6][0] = WHITE;
  pixels[7][0] = WHITE;
}
void pm() {
  pixels[9][0] = WHITE;
  pixels[10][0] = WHITE;
}
/*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/


/**********************************************
   Buttons:
  VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV*/
/** determines whether to display the button menu */
bool inButtonMode() {
  return BUTTON_MODE != -1;
}

/** Switches on the BUTTON_MODE to display the desired menu */
void renderButtonMode() {
  switch (BUTTON_MODE) {
    case 0: renderMinuteMenu(); break;
    case 1: // hour button
      // Now we know which hour (0-23) we're gonna show.
      // Hours 0-11 are am, 12-23 are pm:
      //if (hour % 24 < 12) am();
      //else pm();
      renderHour(hour);
      break;
    case 2: renderDayMenu(); break;
    case 3: renderMonthMenu(); break;
    case 4: renderYearMenu(); break;
    case 5:
      all();
      draw();
      break;
  }
}

/** checks the button state for all the buttons*/
void readButtonStates() {
  for (int i = 0; i < NUM_BUTTONS; i++) {
    readButtonState(i);
  }
}

/** Takes in a buttonIndex, and updates appropriate state according to whether that button:
    -- isn't currently pressed:
       marks it as released, and updates interaction time if this is the first time we noticed they released
    -- is currently pressed:
       TODO choange to:
       -- if middle button is pressed, cycle through modes
       -- if in a mode, up button increases, down button decreases

       from:
       -- if the user is in that mode for the button, then perform the action
       -- otherwise, puts us into that mode
       -- either way, marks the button as pressed and updates last interaction time
*/

long lastInteractionTime[NUM_BUTTONS] = { -1, -1, -1};
boolean buttonsHeld[NUM_BUTTONS] = {false, false, false};

void readButtonState(int buttonIndex) {
  int buttonVal = digitalRead(buttons[buttonIndex]);
  unsigned long now = millis();

  // the button is not on
  if (buttonVal == LOW) {
    // check if it was being held:
    if (buttonsPressed[buttonIndex]) {
      lastInteraction = now;
      lastInteractionTime[buttonIndex] = now;
    }
    buttonsPressed[buttonIndex] = false;
    buttonsHeld[buttonIndex] = false;
  } else { // button is pressed
    // is this is a brand new press?
    if (!buttonsPressed[buttonIndex]) {
      lastInteraction = now;
      lastInteractionTime[buttonIndex] = now;
      newPerformButtonActions(buttonIndex);
      off(); // consider anyButtonPressed
    } else {
      // ok, it was pressed before, maybe it's held:
      if (now - lastInteractionTime[buttonIndex] > 2000) {
        lastInteraction = now;
        lastInteractionTime[buttonIndex] = now;
        buttonsHeld[buttonIndex] = true;
      }

      if (buttonsHeld[buttonIndex]) {
        // if up or down?
        // split out here because there needs to be repeated effects with ratelimiting here.
        newPerformButtonActions(buttonIndex);
      }
    }
    buttonsPressed[buttonIndex] = true;
  }
}



void newPerformButtonActions(int buttonIndex) {
  switch (buttonIndex) {
    case 0:
      performButtonAction(-1);
      break;
    case 1:
      BUTTON_MODE = (BUTTON_MODE + 1) % BUTTON_MODES;
      break;
    case 2:
      performButtonAction(1);
      break;
  }
}

/** Take the action of whatever button mode we're in */
void performButtonAction(int delta) {
  int monthDays = daysInMonth(month, year);
  
  if (BUTTON_MODE == 0) advanceMinute(delta);
  else if (BUTTON_MODE == 1) advanceHour(delta);  
  else if (BUTTON_MODE == 2) day = (day + delta + monthDays) % monthDays;
  else if (BUTTON_MODE == 3) month = (month + delta + 12) % 12;  
  else if (BUTTON_MODE == 4) year += delta;
  else if (BUTTON_MODE == 5) {
    persistAll();
    testDone = false;
  }
  
  setTime(); 
  /* 
  switch (BUTTON_MODE) {
    case 0: // year button
      Serial.println("WHATTTTTT 0");
      Serial.print(BUTTON_MODE);
      Serial.println(" <-- BUTTON_MODE");
      year += delta;
      break;
    case 1: // month button
      Serial.println("WHATTTTTT 1");
      Serial.print(BUTTON_MODE);
      Serial.println(" <-- BUTTON_MODE");
      month = (month + delta + 12) % 12;
      break;
    case 2: // day button
      Serial.println("WHATTTTTT 2");
      Serial.print(BUTTON_MODE);
      Serial.println(" <-- BUTTON_MODE");
      int monthDays = daysInMonth(month, year);
      day = (day + delta + monthDays) % monthDays;
      break;
    case 3: // hour button
      Serial.println("WHATTTTTT 3");
      todaySoFar += 10;
      //      Serial.print(BUTTON_MODE);
      //      Serial.println(" <-- CASE3");
      //      todaySoFar = todaySoFar + MS_PER_HOUR * delta;
      //      hour = (todaySoFar / MS_PER_HOUR) % HOURS_PER_DAY;
      break;
    case 4: // minute button
      Serial.println("WHATTTTTT 4");
      Serial.print(BUTTON_MODE);
      Serial.println(" <-- BUTTON_MODE");
      todaySoFar = todaySoFar + MS_PER_MINUTE * delta;
      minute = (todaySoFar / MS_PER_MINUTE) % MINUTES_PER_HOUR;
      break;
    case 5:
      Serial.println("WHATTTTTT 5");
      Serial.print(BUTTON_MODE);
      Serial.println(" <-- BUTTON_MODE");
      testDone = false;
      break;
    default:
      Serial.println("WHATTTTTT default");
      break;
  }
  Serial.println("WHAT!?");
  */
}


/** Checks to see if they haven't interacted in a while, and turns off button mode if so.*/
void checkButtonIdle() {
  if (lastInteraction != -1 && millis() - lastInteraction > 6000) {
    Serial.println("button timeout");
    lastInteraction = -1;
    BUTTON_MODE = -1;
  }
}

/** Menu display functions: */
void renderMinuteMenu() {
  int displayMinute = minute;

  // if we're not on a five-minute mark:
  if (minute % 5 != 0) {
    around();
  }

  // First, determine whether we are past halfway and need to display "til"
  // information and advance the display hour and subtract out display minutes:

  // if we've passed the halfway mark (32 minutes past is still "around half"):
  if (minute > 32) {
    displayMinute = 60 - minute;

    // display til if we aren't real close to the next hour:
    if (minute < 58) {
      til();
    }
  }
  // otherwise, the normal "past" case, if we aren't "around" the hour
  else if (minute > 2) {
    past();
  }


  if (minute > 32) {
    displayMinute = 60 - minute;
  }
  renderMinute(displayMinute);
  for (int i = 0; i < minute; i++) {
    pixels[i / 5][i % 5] = WHITE;
  }
}
void renderDayMenu() {
  for (int i = 0; i < day; i++) {
    pixels[1 + i % 10][11 - (i / 10)] = WHITE;
  }
}
void renderMonthMenu() {
  switch (month) {
    case 1:
      january(); break;
    case 2:
      february(); break;
    case 3:
      march(); break;
    case 4:
      april(); break;
    case 5:
      may(); break;
    case 6:
      june(); break;
    case 7:
      july(); break;
    case 8:
      august(); break;
    case 9:
      september(); break;
    case 10:
      october(); break;
    case 11:
      november(); break;
    case 12:
      december(); break;
  }
}
void renderYearMenu() {
  int modYear = 2000 + year;
  for (int place = 3; place >= 0; place--) {
    int digit = modYear % 10;
    for (int x = 0; x < 3; x++) {
      for (int y = 0; y < digit; y++) {
        pixels[x + 3 * place][y] = WHITE;
      }
    }
    modYear = modYear / 10;
  }
}

/** Month words: */
void january() {
  pixels[2][7] = WHITE;  // J
  pixels[4][8] = WHITE;  // A
  pixels[5][7] = WHITE;  // N
  pixels[6][7] = WHITE;  // U
  ary();
}
void february() {
  pixels[0][2] = WHITE;  // F
  pixels[1][5] = WHITE;  // E
  pixels[2][5] = WHITE;  // B
  pixels[5][8] = WHITE;  // R
  pixels[6][7] = WHITE;  // U
  ary();
}
void march() {
  pixels[3][7] = WHITE;  // M
  pixels[4][8] = WHITE;  // A
  pixels[5][8] = WHITE;  // R
  pixels[8][6] = WHITE;  // C
  pixels[9][3] = WHITE;  // H
}
void april() {
  pixels[1][6] = WHITE;  // A
  pixels[4][6] = WHITE;  // P
  pixels[5][8] = WHITE;  // R
  pixels[6][6] = WHITE;  // I
  pixels[7][6] = WHITE;  // L
}
void may() {
  pixels[3][7] = WHITE;  // M
  pixels[6][9] = WHITE;  // A
  pixels[11][3] = WHITE; // Y
}
void june() {
  pixels[2][7] = WHITE;  // J
  pixels[3][8] = WHITE;  // U
  pixels[5][7] = WHITE;  // N
  pixels[8][7] = WHITE;  // E
}
void july() {
  pixels[2][7] = WHITE;  // J
  pixels[6][7] = WHITE;  // U
  pixels[7][6] = WHITE;  // L
  pixels[11][3] = WHITE; // Y
}
void august() {
  pixels[0][4] = WHITE;  // A
  pixels[2][2] = WHITE;  // U
  pixels[3][0] = WHITE;  // G
  pixels[6][7] = WHITE;  // U
  pixels[7][4] = WHITE;  // S
  pixels[10][3] = WHITE; // T
}
void september() {
  pixels[2][6] = WHITE;  // S
  pixels[3][5] = WHITE;  // E
  pixels[4][6] = WHITE;  // P
  pixels[5][6] = WHITE;  // T
  ember();
}
void october() {
  pixels[0][0] = WHITE;  // O
  pixels[4][0] = WHITE;  // C
  pixels[6][8] = WHITE;  // T
  pixels[7][9] = WHITE;  // O
  ber();
}
void november() {
  pixels[0][5] = WHITE;  // N
  pixels[1][2] = WHITE;  // O
  pixels[3][1] = WHITE;  // V

  // We don't use the ember() call here
  // because this 'e' is better placed
  // for this word.
  pixels[4][1] = WHITE;  // E
  pixels[7][0] = WHITE;  // M
  ber();
}
void december() {
  pixels[2][11] = WHITE; // D
  pixels[3][5] = WHITE;  // E
  pixels[4][0] = WHITE;  // C
  ember();
}
/** Month word helpers: */
void ary() {
  pixels[8][9] = WHITE;  // A
  pixels[10][8] = WHITE; // R
  pixels[11][3] = WHITE; // Y
}
void ember() {
  pixels[6][3] = WHITE;  // E
  pixels[7][0] = WHITE;  // M
  ber();
}
void ber() {
  pixels[9][8] = WHITE;  // B
  pixels[10][9] = WHITE; // E
  pixels[11][10] = WHITE;// R
}
/*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/


/**********************************************
   Happy Birthday
  VVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVVV*/
/** determines whether it's someone's birthday */
bool inBirthdayMode() {
  return
    (day == 15 && month == 2) ||
    (day == 26 && month == 3) ||
    (day == 28 && month == 5) ||
    (day == 3 && month == 7) ||
    (day == 29 && month == 7) ||
    (day == 8 && month == 9)
    ;
}

/** switches on day/month state and choose what birthday message to display */
void renderBirthday() {
  happyBirthday();
  if (day == 15 && month == 2) whitaker();
  else if (day == 26 && month == 3) hannah();
  else if (day == 28 && month == 5) ross();
  else if (day == 3 && month == 7) robyn();
  else if (day == 29 && month == 7) tyler();
  else if (day == 8 && month == 9) laurie(WHITE);
}

/** birthday words: */
void happyBirthday() {
  happy(WHITE);
  birthday(WHITE);
  // exclam
  pixels[7][1] = WHITE;
}

uint32_t dc(uint32_t color) {
  if (color == MAX) {
    return WHITE;
  }
  return color;
}

void happy(uint32_t color) {
  color = dc(color);
  // happy
  pixels[4][9] = color;
  pixels[4][8] = color;
  pixels[4][6] = color;
  pixels[4][4] = color;
  pixels[4][2] = color;
}

void birthday(uint32_t color) {
  color = dc(color);
  // birthday
  pixels[5][11] = color;
  pixels[5][9] = color;
  pixels[5][8] = color;
  pixels[5][6] = color;
  pixels[5][5] = color;
  pixels[5][4] = color;
  pixels[5][2] = color;
  pixels[5][0] = color;
}


void whitaker() {
  pixels[1][10] = WHITE; // W
  pixels[4][9] = WHITE;  // H
  pixels[5][9] = WHITE;  // I
  pixels[6][8] = WHITE;  // T
  pixels[0][4] = WHITE;  // A
  pixels[2][3] = WHITE;  // K
  pixels[4][1] = WHITE;  // E
  pixels[8][0] = WHITE;  // R
}
void hannah() {
  pixels[0][9] = WHITE;  // H
  pixels[0][8] = WHITE;  // A
  pixels[0][7] = WHITE;  // N
  pixels[0][5] = WHITE;  // N
  pixels[0][4] = WHITE;  // A
  pixels[0][3] = WHITE;  // H
}
void ross() {
  pixels[7][11] = WHITE; // R
  pixels[7][9] = WHITE;  // O
  pixels[7][4] = WHITE;  // S
  pixels[7][1] = WHITE;  // S
}
void robyn() {
  pixels[11][10] = WHITE; // R
  pixels[11][8] = WHITE;  // O
  pixels[11][7] = WHITE;  // B
  pixels[11][3] = WHITE;  // Y
  pixels[11][0] = WHITE;  // N
}
void tyler() {
  pixels[1][11] = WHITE; // T
  pixels[1][8] = WHITE;  // Y
  pixels[1][7] = WHITE;  // L
  pixels[1][5] = WHITE;  // E
  pixels[1][3] = WHITE;  // R
}
void laurie(uint32_t color) {
  color = dc(color);

  pixels[6][10] = color; // L
  pixels[6][9] = color;  // A
  pixels[6][7] = color;  // U
  pixels[6][5] = color;  // R
  pixels[6][4] = color;  // I
  pixels[6][3] = color;  // E
}
/*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/




// Some functions of our own for creating animated effects -----------------

// Fill strip pixels one after another with a color. Strip is NOT cleared
// first; anything there will be covered pixel by pixel. Pass in color
// (as a single 'packed' 32-bit value, which you can get by calling
// strip.Color(red, green, blue) as shown in the loop() function above),
// and a delay time (in milliseconds) between pixels.
void colorWipe(uint32_t color, int wait) {
  for(int i=0; i<strip.numPixels(); i++) { // For each pixel in strip...
    strip.setPixelColor(i, color);         //  Set pixel's color (in RAM)
    strip.show();                          //  Update strip to match
    delay(wait);                           //  Pause for a moment
  }
}

// Theater-marquee-style chasing lights. Pass in a color (32-bit value,
// a la strip.Color(r,g,b) as mentioned above), and a delay time (in ms)
// between frames.
void theaterChase(uint32_t color, int wait) {
  for(int a=0; a<10; a++) {  // Repeat 10 times...
    for(int b=0; b<3; b++) { //  'b' counts from 0 to 2...
      strip.clear();         //   Set all pixels in RAM to 0 (off)
      // 'c' counts up from 'b' to end of strip in steps of 3...
      for(int c=b; c<strip.numPixels(); c += 3) {
        strip.setPixelColor(c, color); // Set pixel 'c' to value 'color'
      }
      strip.show(); // Update strip with new contents
      delay(wait);  // Pause for a moment
    }
  }
}

// Rainbow cycle along whole strip. Pass delay time (in ms) between frames.
void rainbow(int wait) {
  // Hue of first pixel runs 5 complete loops through the color wheel.
  // Color wheel has a range of 65536 but it's OK if we roll over, so
  // just count from 0 to 5*65536. Adding 256 to firstPixelHue each time
  // means we'll make 5*65536/256 = 1280 passes through this outer loop:
  for(long firstPixelHue = 0; firstPixelHue < 5*65536; firstPixelHue += 256) {
    for(int i=0; i<strip.numPixels(); i++) { // For each pixel in strip...
      // Offset pixel hue by an amount to make one full revolution of the
      // color wheel (range of 65536) along the length of the strip
      // (strip.numPixels() steps):
      int pixelHue = firstPixelHue + (i * 65536L / strip.numPixels());
      // strip.ColorHSV() can take 1 or 3 arguments: a hue (0 to 65535) or
      // optionally add saturation and value (brightness) (each 0 to 255).
      // Here we're using just the single-argument hue variant. The result
      // is passed through strip.gamma32() to provide 'truer' colors
      // before assigning to each pixel:
      strip.setPixelColor(i, strip.gamma32(strip.ColorHSV(pixelHue)));
    }
    strip.show(); // Update strip with new contents
    delay(wait);  // Pause for a moment
  }
}

// Rainbow-enhanced theater marquee. Pass delay time (in ms) between frames.
void theaterChaseRainbow(int wait) {
  int firstPixelHue = 0;     // First pixel starts at red (hue 0)
  for(int a=0; a<30; a++) {  // Repeat 30 times...
    for(int b=0; b<3; b++) { //  'b' counts from 0 to 2...
      strip.clear();         //   Set all pixels in RAM to 0 (off)
      // 'c' counts up from 'b' to end of strip in increments of 3...
      for(int c=b; c<strip.numPixels(); c += 3) {
        // hue of pixel 'c' is offset by an amount to make one full
        // revolution of the color wheel (range 65536) along the length
        // of the strip (strip.numPixels() steps):
        int      hue   = firstPixelHue + c * 65536L / strip.numPixels();
        uint32_t color = strip.gamma32(strip.ColorHSV(hue)); // hue -> RGB
        strip.setPixelColor(c, color); // Set pixel 'c' to value 'color'
      }
      strip.show();                // Update strip with new contents
      delay(wait);                 // Pause for a moment
      firstPixelHue += 65536 / 90; // One cycle of color wheel over 90 frames
    }
  }
}
