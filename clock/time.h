/**********************************************
 * time globals
 *********************************************/
 
#include "RTClib.h"

#define MS_PER_SECOND      1000
#define SECONDS_PER_MINUTE 60
#define MINUTES_PER_HOUR   60
#define HOURS_PER_DAY      24
// DAYS_PER_MONTH
#define MONTHS_PER_YEAR    12

uint16_t MS_PER_MINUTE = MS_PER_SECOND * SECONDS_PER_MINUTE; // 60,000
uint32_t MS_PER_HOUR   = MS_PER_MINUTE * MINUTES_PER_HOUR;   // 3,600,000
uint32_t MS_PER_DAY    = MS_PER_HOUR   * HOURS_PER_DAY;      // 86,400,000

#define AROUND_OFFSET   2
#define HALF_AN_HOUR   30

RTC_DS3231 rtc;
DateTime now;

/** return the number of days in the given month of the given year */
uint8_t dim[13] = {
   0, // unused
  31, // jan
  28, // feb
  31, // mar
  30, // apr
  31, // may
  30, // jun
  31, // jul
  31, // aug
  30, // sept
  31, // oct
  30, // nov
  31  // dec
};
