/***********************************************
 * rendering the demo
/*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/

#define BLOCK_TOTAL   1500
#define HUE_ADVANCE    500
#define SQUARE_ADVANCE 700

uint8_t _sqInward     = 0;
uint8_t _sqOutward    = 0;
uint8_t _fillInward   = 0;
uint8_t _fillOutward  = 0;
uint8_t _drainInward  = 0;
uint8_t _drainOutward = 0;

uint32_t _baseHue     = 0;

void demoSetup() {
  printTime("demo:setup");
  off();
  demoInProgress = false; 
  phase = 0;

  _sqInward     = 0;
  _sqOutward    = 0;
  _fillInward   = 0;
  _fillOutward  = 0;
  _drainInward  = 0;  
  _drainOutward = 0;
}

void demoLoop(uint32_t diff) {  
  switch (phase) {
    case 0:  renderBlank(diff, 1000); break;
    case 1:  _renderFillOutward(diff, 4, 0); break;
    case 2:  _renderDrainInward(diff, 4, 2); break;
    case 3:  _renderFillInward(diff, 3, 0); break;
    case 4:  _renderDrainOutward(diff, 3, 3); break;
    case 5:  renderBlank(diff, 2000); break;
    
    case 6:  _renderSquaresOutward(diff, 1); break;
    case 7:  _renderSquaresInward(diff, 2); break;
    case 8:  _renderSquaresOutward(diff, 3); break;
    case 9:  _renderSquaresInward(diff, 4); break;
    case 10: _renderSquaresOutward(diff, 5); break;
    case 11: _renderSquaresInward(diff, 6); break;
    case 12: renderBlank(diff, 500); break;
    
    case 13: _renderFillOutward(diff, 6, 0); break;
    case 14: _renderBlock(diff, 2000); break;
    
    case 15: _renderDrainOutward(diff, 6, 0); break;
    case 16: renderBlank(diff, 700); break;
    
    case 17: _renderFillInward(diff, 6, 0); break;
    case 18: _renderBlock(diff, 2000); break;
    
    case 19: _renderDrainInward(diff, 6, 0); break;
    case 20: _renderFillOutward(diff, 6, 0); break;
    case 21: _renderBlock(diff, 2000); break;
    
    case 22: _renderDrainOutward(diff, 6, 0); break;
    case 23: renderBlank(diff, 700); break;
    case 24: _renderFillOutward(diff, 6, 0); break;
    case 25: _renderBlock(diff, 2000); break;
    
    case 26: _renderDrainOutward(diff, 6, 0); break;
    default: demoSetup();
  }
}

void enterDemo() {
  printTime("demo:enter");
  demoInProgress = true;
}

/***********************************************
 * demo helpers
/*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/
void _advanceColor() {
  _baseHue += HUE_ADVANCE;
}

int _sqHue(int i) {
  return _baseHue + i * SQUARE_ADVANCE;
}

/***********************************************
 * render phases
/*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/
void _renderBlock(uint32_t diff, uint32_t duration) {
  uint32_t phaseDiff = millis() - phaseEnteredMillis;
  if (phaseDiff > duration) {
    advance();
    return;
  }

  if (!shouldRender(diff, DEFAULT_FS)) return;
  for (int i = 0; i < HALFWAY_COUNT; i++) {
    renderSquare(i);
  }
  _advanceColor();
  render(DEFAULT_FS);
}

void _renderSquaresInward(uint32_t diff, int count) {
  if (_sqInward > count) {
    _sqInward = 0;
    advance();
    return;
  }
  if (!shouldRender(diff, DEFAULT_FS)) return;

  off();
  int offset = HALFWAY_COUNT - count;
  renderSquare(_sqInward + offset);
  
  _sqInward++;
  _advanceColor();
  render(DEFAULT_FS);
}

void _renderSquaresOutward(uint32_t diff, int count) {
  if (_sqOutward > count) {
    _sqOutward = 0;
    advance();
    return;
  }

  if (!shouldRender(diff, DEFAULT_FS)) return;

  off();
  renderSquare(HALFWAY_COUNT - _sqOutward);
  
  _sqOutward++;
  _advanceColor();
  render(DEFAULT_FS);
}

void _renderFillInward(uint32_t diff, int count, int offset) {
  if (_fillInward >= count) {
    _fillInward = 0;
    advance();
    return;
  }

  if (!shouldRender(diff, DEFAULT_FS)) return;

  off();
  for (int i = 0; i <= _fillInward; i++) renderSquare(i + offset);
  
  _fillInward++;
  _advanceColor();
  render(DEFAULT_FS);
}

void _renderFillOutward(uint32_t diff, int count, int offset) {
  if (_fillOutward >= count) {
    _fillOutward = 0;
    advance();
    return;
  }

  if (!shouldRender(diff, DEFAULT_FS)) return;

  off();
  int squares = _fillOutward + offset;
  for (int i = 0; i <= squares; i++) renderSquare(HALFWAY_IDX - i);   
  
  _fillOutward++;
  _advanceColor();
  render(DEFAULT_FS);
}

void _renderDrainInward(uint32_t diff, int count, int offset) {
  if (_drainInward >= count) {
    _drainInward = 0;
    advance();
    return;
  }

  if (!shouldRender(diff, DEFAULT_FS)) return;
  
  off();
  int squares = HALFWAY_COUNT - _drainInward - offset;
  for (int i = 0; i <= squares; i++) renderSquare(HALFWAY_COUNT - i);   
  
  _drainInward++;
  _advanceColor();
  render(DEFAULT_FS);
}

void _renderDrainOutward(uint32_t diff, int count, int offset) {
  if (_drainOutward >= count) {
    _drainOutward = 0;
    advance();
    return;
  }

  if (!shouldRender(diff, DEFAULT_FS)) return;

  off();
  int squares = HALFWAY_IDX - _drainOutward - offset;
  for (int i = 0; i <= squares; i++) renderSquare(i);
  
  _drainOutward++;
  _advanceColor();
  render(DEFAULT_FS);
}
