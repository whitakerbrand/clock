/***********************************************
 * Name words:
/*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/

void whitaker(uint32_t color) {
  pixels[1][10] = color; // W
  pixels[4][9]  = color; // H
  pixels[5][9]  = color; // I
  pixels[6][8]  = color; // T
  pixels[0][4]  = color; // A
  pixels[2][3]  = color; // K
  pixels[4][1]  = color; // E
  pixels[8][0]  = color; // R
}

void hannah(uint32_t color) {
  pixels[0][9] = color;  // H
  pixels[0][8] = color;  // A
  pixels[0][7] = color;  // N
  pixels[0][5] = color;  // N
  pixels[0][4] = color;  // A
  pixels[0][3] = color;  // H
}

void ross(uint32_t color) {
  pixels[7][11] = color; // R
  pixels[7][9]  = color; // O
  pixels[7][4]  = color; // S
  pixels[7][1]  = color; // S
}

void robyn(uint32_t color) {
  pixels[11][10] = color; // R
  pixels[11][8]  = color; // O
  pixels[11][7]  = color; // B
  pixels[11][3]  = color; // Y
  pixels[11][0]  = color; // N
}

void tyler(uint32_t color) {
  pixels[1][11] = color; // T
  pixels[1][8]  = color; // Y
  pixels[1][7]  = color; // L
  pixels[1][5]  = color; // E
  pixels[1][3]  = color; // R
}

void laurie(uint32_t color) {
  pixels[6][10] = color; // L
  pixels[6][9]  = color; // A
  pixels[6][7]  = color; // U
  pixels[6][5]  = color; // R
  pixels[6][4]  = color; // I
  pixels[6][3]  = color; // E
}
