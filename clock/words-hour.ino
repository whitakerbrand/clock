/***********************************************
 * hour words:
/*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/

void hourOne() {
  for (int x = 0; x <= 2; x++) pixels[x][0] = WHITE;
}

void hourTwo() {
  for (int x = 1; x <= 3; x++) pixels[x][4] = WHITE;
}

void hourThree() {
  for (int x = 4; x <= 8; x++) pixels[x][5] = WHITE;
}

void hourFour() {
  for (int x = 0; x <= 3; x++) pixels[x][2] = WHITE;
}

void hourFive() {
  for (int x = 3; x <= 6; x++) pixels[x][3] = WHITE;
}

void hourSix() {
  for (int x = 9; x <= 11; x++) pixels[x][5] = WHITE;
}

void hourSeven() {
  for (int x = 7; x <= 11; x++) pixels[x][4] = WHITE;
}

void hourEight() {
  for (int x = 6; x <= 10; x++) pixels[x][3] = WHITE;
}

void hourNine() {
  for (int x = 8; x <= 11; x++) pixels[x][1] = WHITE;
}

void hourTen() {
  for (int x = 9; x <= 11; x++) pixels[x][6] = WHITE;
}

void hourEleven() {
  for (int x = 0; x <= 5; x++) pixels[x][1] = WHITE;
}

void hourTwelve() {
  for (int x = 6; x <= 11; x++) pixels[x][2] = WHITE;
}

void am() {
  pixels[6][0] = WHITE;
  pixels[7][0] = WHITE;
}

void pm() {
  pixels[9][0] = WHITE;
  pixels[10][0] = WHITE;
}
