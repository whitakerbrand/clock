/***********************************************
 * birtthday words:
/*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/

void happyBirthday(uint32_t color) {
  happy(color);
  birthday(color);
}

void happy(uint32_t color) {
  pixels[4][9] = color;
  pixels[4][8] = color;
  pixels[4][6] = color;
  pixels[4][4] = color;
  pixels[4][2] = color;
}

void birthday(uint32_t color) {
  pixels[5][11] = color;
  pixels[5][9] = color;
  pixels[5][8] = color;
  pixels[5][6] = color;
  pixels[5][5] = color;
  pixels[5][4] = color;
  pixels[5][2] = color;
  pixels[5][0] = color;
}

void exclam(uint32_t color) {
  pixels[6][1] = color;
}
