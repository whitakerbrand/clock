#include "time.h"
#include "matrix.h"

#include "buttons.h"
#include "demo.h"
#include "render.h"
#include "startup.h"

#define DEBUG true
#define WAIT_FOR_SERIAL true

uint32_t phaseEnteredMillis;
uint8_t  phase;
uint32_t lastRenderMillis;

void setup() {
  utilSetup();
  timeSetup();
  timeLoop(); // primetime
  
  off();
  matrixSetup();

  startupSetup();
  demoSetup();
  enterStartup();
}

void loop() {
  timeLoop();
  buttonLoop();

  uint32_t diff = millis() - lastRenderMillis;

       if (startupInProgress)    startupLoop(diff);
  else if (demoInProgress)       demoLoop(diff);
  else if (menuInProgress())     menuLoop(diff);
  else if (birthdayInProgress()) birthdayLoop(diff);
  else                           clockLoop(diff);
}
