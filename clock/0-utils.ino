/***********************************************
 * random stuff, state and framesize related
/*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/

void utilSetup() {
  if (DEBUG) {
    Serial.begin(38400);
    Serial.println("setup");
    
#ifndef ESP8266
    if (WAIT_FOR_SERIAL) while (millis() > 5000 && !Serial); // wait for serial port to connect. Needed for native USB
#endif
  }
}

/** move to the next phase */
void advance() {
  printTime("advance");
  phaseEnteredMillis = millis();
  phase++;
}

char buf[64];
void printTime(char* label) {
  if  (DEBUG) {
    sprintf(buf, "p:%d\t bl:%d\t -- %d %d/%d %d:%d:%d --\t %s", phase, brightnessLevel, now.day(), now.month(), now.year(), now.hour(), now.minute(), now.second(), label);
    Serial.println(buf);    
  }
}
