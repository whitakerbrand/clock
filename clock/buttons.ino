/**********************************************
 * button stuff
/*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/

/** checks the button state for all the buttons */
void buttonLoop() {
  for (int i = 0; i < NUM_BUTTONS; i++) {
    readButtonState(i);
  }
  checkButtonIdle();
}

/**********************************************
 * local button state
/*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/
uint8_t buttonPins[NUM_BUTTONS] = { A0, A1, A2 };

int8_t buttonMode = -1;
uint32_t lastInteraction = -1;
uint32_t lastInteractionTime[NUM_BUTTONS];
bool buttonsHeld[NUM_BUTTONS];

/** Updates state for the given button */
void readButtonState(int buttonIndex) {
  int buttonVal = digitalRead(buttonPins[buttonIndex]);
  unsigned long nowMillis = millis();

  if (buttonVal == LOW) {
    // check if this a held button being released
    if (buttonsHeld[buttonIndex]) {
      lastInteraction = nowMillis;
      lastInteractionTime[buttonIndex] = nowMillis;
    }
    buttonsHeld[buttonIndex] = false;
  } else { // buttonVal == HIGH  
    if (!buttonsHeld[buttonIndex]) {
      buttonAction(buttonIndex);
      
      lastInteraction = nowMillis;
      lastInteractionTime[buttonIndex] = nowMillis;
    } 
    // ok, it was held before, maybe it's repeating:
    else if (nowMillis - lastInteractionTime[buttonIndex] > BUTTON_HOLD_DELAY) {
      buttonAction(buttonIndex);
      lastInteraction = nowMillis;
      lastInteractionTime[buttonIndex] += BUTTON_REPEAT_DELAY;
    }
    buttonsHeld[buttonIndex] = true;
  }
}

/* do the thing represented by the given button */
void buttonAction(int buttonIndex) {
  switch (buttonIndex) {
    case 0: adjust(-1); break;
    case 1: buttonMode = (buttonMode + 1) % BUTTON_MODES; break;
    case 2: adjust(1); break;
  }
}

/** Take the action of whatever button mode we're in */
void adjust(int delta) {
  int daysThisMonth = DAYS_PER_MONTH(now.month(), now.year());

  switch(buttonMode) {
    case 0: adjustMinute(delta); break;
    case 1: adjustHour(delta); break;
    case 2: adjustDay(delta); break;
    case 3: adjustMonth(delta); break;
    case 4: adjustMonth(delta); break;
    case 5: adjustBrightness(delta); break;
    case 6: enterDemo(); break;
  }
}

/** change only the denomination in question, leaving the others alone: */
void adjustMinute(int delta) {   
  uint8_t minute = (now.minute() + delta + MINUTES_PER_HOUR) % MINUTES_PER_HOUR;
  rtc.adjust(DateTime(now.year(), now.month(), now.day(), now.hour(), minute, now.second()));
}

void adjustHour(int delta) {
  uint8_t hour = (now.hour() + delta + HOURS_PER_DAY) % HOURS_PER_DAY;
  rtc.adjust(DateTime(now.year(), now.month(), now.day(), hour, now.minute(), now.second()));
}

void adjustDay(int delta) {
  uint8_t daysThisMonth = DAYS_PER_MONTH(now.month(), now.year());
  uint8_t day = (now.day() + delta + daysThisMonth) % daysThisMonth;
  rtc.adjust(DateTime(now.year(), now.month(), day, now.hour(), now.minute(), now.second()));
}

void adjustMonth(int delta) {
  uint8_t month = (now.month() + delta + MONTHS_PER_YEAR) % MONTHS_PER_YEAR;
  rtc.adjust(DateTime(now.year(), month, now.day(), now.hour(), now.minute(), now.second()));
}

void adjustYear(int delta) {
  uint8_t year = now.year() + delta;
  rtc.adjust(DateTime(year, now.month(), now.day(), now.hour(), now.minute(), now.second()));
}

/** Checks to see if they haven't interacted in a while, and turns off button mode if so.*/
void checkButtonIdle() {
  if (lastInteraction != -1 && millis() - lastInteraction > BUTTON_IDLE_TIMEOUT) {
    printTime("btn timeout");
    lastInteraction = -1;
    buttonMode = -1;
  }
}
