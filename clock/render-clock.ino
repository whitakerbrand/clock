/***********************************************
 * rendering the clock
/*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/

#define CLOCK_FRAMESIZE 400

void clockLoop(uint32_t diff) {
  if (!shouldRender(diff, CLOCK_FRAMESIZE)) return;
  
  off();
  itIs();
  _renderClock(now.minute(), now.hour());
  render(CLOCK_FRAMESIZE);
}

/** figure out what hours/minute words to display and display them */
void _renderClock(uint8_t minute, uint8_t hour) {
  uint8_t displayMinute = minute;
  uint8_t displayHour = hour;

  // if we're not on a five-minute mark:
  if (minute % 5 != 0) {
    around();
  }

  // First, determine whether we are past halfway and need to display "til"
  // and adjust the display hour and subtract out display minutes:

  // if we've passed the halfway mark (32 minutes past is still "around half"):
  if (minute > AROUND_OFFSET + HALF_AN_HOUR) {
    displayHour = (displayHour + 1) % HOURS_PER_DAY;
    displayMinute = 60 - minute;

    // display til if we aren't real close to the next hour:
    if (minute < MINUTES_PER_HOUR - AROUND_OFFSET) {
      til();
    }
  }
  // otherwise, the normal "past" case, if we aren't "around" the hour
  else if (minute > AROUND_OFFSET) {
    past();
  }
  
  // turn the right pixels on
  renderMinute(displayMinute);
  renderHour(displayHour);
}
