/***********************************************
 * rendering the startup sequence
/*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/

#define STARTUP_PHASES 5
#define STARTUP_SPIRAL_FS 20
#define STARTUP_SPIRAL_TOTAL 144
#define STARTUP_HBDAY_TOTAL 2000
#define STARTUP_LAURIE_TOTAL 2000
#define STARTUP_FADE_TOTAL 3000

void startupSetup() {
  printTime("startup:setup");
  startupInProgress = false;
  phase = 0;
}

void startupLoop(uint32_t diff) {
  switch (phase) {
    case 0:  _renderFadeIn(diff); break;
    case 1:  _renderFadeOut(diff); break;
    case 2:  _renderSpiral(diff); break;
    case 3:  _renderHbday(diff); break;
    case 4:  _renderLaurie(diff); break;
    default: startupSetup();
  }
}

void enterStartup() {
  printTime("startup:enter");
  startupInProgress = true;
}

/***********************************************
 * render phases
/*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/
void _renderFadeIn(uint32_t diff) {
  if (!shouldRender(diff, DEFAULT_FS)) return;

  uint32_t phaseDiff = millis() - phaseEnteredMillis;  
  if (phaseDiff >= STARTUP_FADE_TOTAL) advance();
  else {
    colorAll(WHITE);
    renderBrightness(1.0 * phaseDiff / STARTUP_FADE_TOTAL);
    render(DEFAULT_FS);
  }
}

void _renderFadeOut(uint32_t diff) {
  if (!shouldRender(diff, DEFAULT_FS)) return;
  
  uint32_t phaseDiff = millis() - phaseEnteredMillis;
  if (phaseDiff >= STARTUP_FADE_TOTAL) advance();
  else {
    colorAll(WHITE);
    renderBrightness(1 - 1.0 * phaseDiff / STARTUP_FADE_TOTAL);
    render(DEFAULT_FS);
  }  
}

void _renderHbday(uint32_t diff) {  
  if (!shouldRender(diff, DEFAULT_FS)) return;

  uint32_t phaseDiff = millis() - phaseEnteredMillis;
  if (phaseDiff >= STARTUP_HBDAY_TOTAL) advance();
  else {
    off();
    happy(WHITE);
    birthday(WHITE);
    render(DEFAULT_FS);
  }
}

void _renderLaurie(uint32_t diff) {
  if (!shouldRender(diff, DEFAULT_FS)) return;

  uint32_t phaseDiff = millis() - phaseEnteredMillis;
  if (phaseDiff >= STARTUP_LAURIE_TOTAL) advance();
  else {
    off();
    laurie(RED);
    exclam(WHITE);
    render(DEFAULT_FS);
  }
}

/***********************************************
 * the spiral
/*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/
uint16_t _total0 = 0;
uint8_t _step0 = 1;

uint8_t _startup0X = 0;
uint8_t _startup0Y = 0;

uint8_t _shadow0X = 0;
uint8_t _shadow0Y = 0;

bool _facingSideways = true;
int8_t _xDirection = 1;
int8_t _yDirection = 1;

uint8_t _horizSteps = MAX_COL;
uint8_t _vertSteps = MAX_ROW;

void _renderSpiral(uint32_t diff) {
  if (!shouldRender(diff, STARTUP_SPIRAL_FS)) return;
  
  if (_horizSteps == 0 && _vertSteps == 0) {
    renderBrightness(1.0);
    advance();
    return;
  }
    
  _total0++;
  off();

  _shadow0X = _startup0X;
  _shadow0Y = _startup0Y;

  if (_facingSideways) {
    _startup0X += _xDirection;
    if (_step0 == _horizSteps) {
      _facingSideways = false;
      _xDirection *= -1;
      _step0 = 0;

      // awkward 1-off for the first location (0,0)
      if (_startup0Y != 0) _horizSteps--;
    }
  } else {
    _startup0Y += _yDirection;
    if (_step0 == _vertSteps) {
      _facingSideways = true;
      _yDirection *= -1;
      _vertSteps--;
      _step0 = 0;
      renderBrightness(1.0 * _total0 / STARTUP_SPIRAL_TOTAL);
    }
  }
  _step0++;

  pixels[_shadow0X][_shadow0Y] = RED;
  pixels[_startup0X][_startup0Y] = WHITE;
  pixels[NUM_COLS - 1 - _shadow0X][NUM_ROWS - 1 - _shadow0Y] = RED;
  pixels[NUM_COLS - 1 - _startup0X][NUM_ROWS - 1 - _startup0Y] = WHITE;

  render(STARTUP_SPIRAL_FS);
}
