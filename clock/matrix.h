/***********************************************
 * matrix globals
/*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/

#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
#include <avr/power.h> // Required for 16 MHz Adafruit Trinket
#endif

#define DATA_PIN     4
#define NUM_PIXELS 144

// copypasta
// When setting up the NeoPixel library, we tell it how many pixels,
// and which pin to use to send signals. Note that for older NeoPixel
// strips you might need to change the third parameter -- see the
// strandTest example for more information on possible values.
Adafruit_NeoPixel strip(NUM_PIXELS, DATA_PIN, NEO_GRB + NEO_KHZ800);

/** can't just change these */
#define NUM_ROWS 12
#define NUM_COLS 12

/** one less, for indexes */
#define MAX_ROW 11
#define MAX_COL 11

/** used for some animations */
#define HALFWAY_IDX   5
#define HALFWAY_COUNT 6

/** any pixels on? */
uint32_t pixels[NUM_ROWS][NUM_COLS];

/** how bright is it? */
#define MIN_BRIGHTNESS               5
#define MAX_BRIGHTNESS             100
#define STARTING_BRIGHTNESS         20

#define BRIGHTNESS_LEVEL_MAGNIFIER  10
#define DEFAULT_BRIGHTNESS_LEVEL     5
#define MIN_BRIGHTNESS_LEVEL         0
#define MAX_BRIGHTNESS_LEVEL        12
#define TOTAL_BRIGHTER_HOURS        16
#define MIN_TIME_MODIFIER          0.4
#define START_BRIGHTENING_HOUR       7

uint16_t START_BRIGHTENING_MINUTE = START_BRIGHTENING_HOUR * MINUTES_PER_HOUR;
uint8_t  MID_BRIGHT               = START_BRIGHTENING_MINUTE / 2;
uint8_t  brightnessLevel          = DEFAULT_BRIGHTNESS_LEVEL;

/*****************************************************
 * pixel index by x,y location 
 *   writing out the matrix puts y first, as in _INDEX_MAP[y][x]
 *   so during startup, reflect it over y=x to format it into INDEX_MAP[x][y]
 ****************************************************/
int INDEX_MAP[NUM_ROWS][NUM_COLS];

/** pixel index by y,-x location */
int _INDEX_MAP[NUM_ROWS][NUM_COLS] = {
  {   0,  23,  24,  47,  48,  71,  72,  95,  96, 119, 120, 143 },
  {   1,  22,  25,  46,  49,  70,  73,  94,  97, 118, 121, 142 },
  {   2,  21,  26,  45,  50,  69,  74,  93,  98, 117, 122, 141 },
  {   3,  20,  27,  44,  51,  68,  75,  92,  99, 116, 123, 140 },
  {   4,  19,  28,  43,  52,  67,  76,  91, 100, 115, 124, 139 },
  {   5,  18,  29,  42,  53,  66,  77,  90, 101, 114, 125, 138 },
  {   6,  17,  30,  41,  54,  65,  78,  89, 102, 113, 126, 137 },
  {   7,  16,  31,  40,  55,  64,  79,  88, 103, 112, 127, 136 },
  {   8,  15,  32,  39,  56,  63,  80,  87, 104, 111, 128, 135 },
  {   9,  14,  33,  38,  57,  62,  81,  86, 105, 110, 129, 134 },
  {  10,  13,  34,  37,  58,  61,  82,  85, 106, 109, 130, 133 },
  {  11,  12,  35,  36,  59,  60,  83,  84, 107, 108, 131, 132 },
};

/** colors */
uint32_t OFF   = -1;  // don't send this to the strip
uint32_t WHITE = strip.Color(255, 255, 255);
uint32_t GREEN = strip.Color(255,   0,   0);
uint32_t RED   = strip.Color(  0, 255,   0);
uint32_t BLUE  = strip.Color(  0,   0, 255);
uint32_t PURP  = strip.Color(  0, 255, 255);
uint32_t TEAL  = strip.Color(253,  97, 203);
