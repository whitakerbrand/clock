/***********************************************
 * led matrix stuff
/*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/

void matrixSetup() {
  reflectMap();
  brightnessLevel = DEFAULT_BRIGHTNESS_LEVEL;
  strip.begin(); // INITIALIZE NeoPixel strip object (REQUIRED)
}

/***********************************************
 * Matrix display utilities
/*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/

/** Slow call: write the pixels to the strip */
void render(uint16_t fs) {
  lastRenderMillis += fs;
  for (int y = 0; y < NUM_ROWS; y++) {
    for (int x = 0; x < NUM_COLS; x++) {
      uint32_t color = pixels[x][y];
      if (color != OFF) {
        int index = INDEX_MAP[x][y];
        strip.setPixelColor(index, color);
      }
    }
  }
  strip.show();
}

/** is it time yet? */
bool shouldRender(uint32_t diff, uint16_t fs) {
  return diff >= fs;
}

/** turn on all of the lights: */
void colorAll(uint32_t color) {
  for (int x = 0; x < NUM_COLS; x++) {
    for (int y = 0; y < NUM_ROWS; y++) {
      pixels[x][y] = color;
    }
  }
}

/** turn off all the pixels */
void off() {
  colorAll(OFF);
  strip.clear();
}

/** change the brigntness level up or down */
void adjustBrightness(int delta) {
  brightnessLevel += delta;
  if (brightnessLevel < MIN_BRIGHTNESS) {
    brightnessLevel = MIN_BRIGHTNESS;
  } else if (brightnessLevel > MAX_BRIGHTNESS) {
    brightnessLevel = MAX_BRIGHTNESS;
  }
  renderBrightness(1.0);
}

/** Renders the current brightness to the matrix, adjusted by * perc */
void renderBrightness(float perc) {
  int brightnessMagnitude = brightnessLevel * BRIGHTNESS_LEVEL_MAGNIFIER;
  float adjusted = brightnessMagnitude * perc * _timeBrightnessAdjustment();
  uint8_t brightness = (uint8_t) (adjusted + 0.5); // to round
  strip.setBrightness(brightness);
  strip.show();  
}

/** what percentage brightness should we render given the time of day? */
float _timeBrightnessAdjustment() {
  uint16_t progress = now.hour() * MINUTES_PER_HOUR + now.minute();

  // under halfway? easy percentage
  // after? going from 100% -> 0%, so subtract out progress on the second half:
  float perc = progress < MID_BRIGHT ? perc = 1.0 * progress / MID_BRIGHT
                                     : perc = 1.0 - (progress - MID_BRIGHT) / MID_BRIGHT;
               
  return perc * (1 - MIN_TIME_MODIFIER) + MIN_TIME_MODIFIER;
}
