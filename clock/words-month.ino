/***********************************************
 * month words:
/*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/

void january() {
  pixels[2][7] = WHITE;  // J
  pixels[4][8] = WHITE;  // A
  pixels[5][7] = WHITE;  // N
  pixels[6][7] = WHITE;  // U
  ary();
}

void february() {
  pixels[0][2] = WHITE;  // F
  pixels[1][5] = WHITE;  // E
  pixels[2][5] = WHITE;  // B
  pixels[5][8] = WHITE;  // R
  pixels[6][7] = WHITE;  // U
  ary();
}

void march() {
  pixels[3][7] = WHITE;  // M
  pixels[4][8] = WHITE;  // A
  pixels[5][8] = WHITE;  // R
  pixels[8][6] = WHITE;  // C
  pixels[9][3] = WHITE;  // H
}

void april() {
  pixels[1][6] = WHITE;  // A
  pixels[4][6] = WHITE;  // P
  pixels[5][8] = WHITE;  // R
  pixels[6][6] = WHITE;  // I
  pixels[7][6] = WHITE;  // L
}

void may() {
  pixels[3][7] = WHITE;  // M
  pixels[6][9] = WHITE;  // A
  pixels[11][3] = WHITE; // Y
}

void june() {
  pixels[2][7] = WHITE;  // J
  pixels[3][8] = WHITE;  // U
  pixels[5][7] = WHITE;  // N
  pixels[8][7] = WHITE;  // E
}

void july() {
  pixels[2][7] = WHITE;  // J
  pixels[6][7] = WHITE;  // U
  pixels[7][6] = WHITE;  // L
  pixels[11][3] = WHITE; // Y
}

void august() {
  pixels[0][4] = WHITE;  // A
  pixels[2][2] = WHITE;  // U
  pixels[3][0] = WHITE;  // G
  pixels[6][7] = WHITE;  // U
  pixels[7][4] = WHITE;  // S
  pixels[10][3] = WHITE; // T
}

void september() {
  pixels[2][6] = WHITE;  // S
  pixels[3][5] = WHITE;  // E
  pixels[4][6] = WHITE;  // P
  pixels[5][6] = WHITE;  // T
  ember();
}

void october() {
  pixels[0][0] = WHITE;  // O
  pixels[4][0] = WHITE;  // C
  pixels[6][8] = WHITE;  // T
  pixels[7][9] = WHITE;  // O
  ber();
}

void november() {
  pixels[0][5] = WHITE;  // N
  pixels[1][2] = WHITE;  // O
  pixels[3][1] = WHITE;  // V

  // We don't use the ember() call here
  // because this 'e' is better placed
  // for this word.
  pixels[4][1] = WHITE;  // E
  pixels[7][0] = WHITE;  // M
  ber();
}

void december() {
  pixels[2][11] = WHITE; // D
  pixels[3][5] = WHITE;  // E
  pixels[4][0] = WHITE;  // C
  ember();
}

/** Month word helpers: */
void ary() {
  pixels[8][9] = WHITE;  // A
  pixels[10][8] = WHITE; // R
  pixels[11][3] = WHITE; // Y
}

void ember() {
  pixels[6][3] = WHITE;  // E
  pixels[7][0] = WHITE;  // M
  ber();
}

void ber() {
  pixels[9][8] = WHITE;  // B
  pixels[10][9] = WHITE; // E
  pixels[11][10] = WHITE;// R
}
