/***********************************************
 * button globals
/*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/

#define NUM_BUTTONS 3
#define BUTTON_MODES 7

#define BUTTON_REPEAT_DELAY 200
#define BUTTON_HOLD_DELAY 2000
#define BUTTON_IDLE_TIMEOUT 5000
