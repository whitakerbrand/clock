/***********************************************
 * time stuff
/*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/

void timeSetup() {
  lastRenderMillis = 0;
    
  if (!rtc.begin()) {
    if (DEBUG) {
      Serial.println("Couldn't find RTC");
      Serial.flush();  
    }
    abort();
  }

  if (rtc.lostPower()) {  
    if (DEBUG) {
      Serial.println("RTC lost power, let's set the time!");
      Serial.flush();  
      
    }
    
    // When time needs to be set on a new device, or after a power loss, the
    // following line sets the RTC to the date & time this sketch was compiled
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
    
    // int DEFAULT_YEAR = 2021;
    // int DEFAULT_MONTH = 9;
    // int DEFAULT_DAY = 8;
    // int DEFAULT_MINUTE = 0;
    // int DEFAULT_SECOND = 0;
    // rtc.adjust(DateTime(DEFAULT_YEAR, DEFAULT_MONTH, DEFAULT_DAY, DEFAULT_MINUTE, DEFAULT_SECOND);
  }
}

/** To call repeatedly: */
uint8_t _second; // just for detecting changes
uint8_t _minute; // just for detecting changes
void timeLoop() {
  now = rtc.now();
  if (_second != now.second()) tick();
  if (_minute != now.minute()) minuteTick();
}

/** happens once a second: */ 
void tick() {
  _second = now.second();

  if (startupInProgress) {
    printTime("startup");
  } else if (demoInProgress) {
    printTime("demo");
  } else if (menuInProgress()) {
    printTime("button");
  } else if (isBirthday() && now.second() % 10 <= 3) {
    printTime("birthday");
  } else {
    printTime("clock");
  }
}

/** once a minute: */ 
void minuteTick() {
  _minute = now.minute();
  renderBrightness(1.0);
}


/** All the other ones have constants, daysPerMonth felt wrang */
int DAYS_PER_MONTH(int month, int year) {
  if (month == 2 &&
      year % 4 == 0 &&
      (year % 100 != 0 || year % 400 == 0)) {
    return dim[month] + 1;
  }
  return dim[month];
}

/** determines whether it's someone's birthday */
bool isBirthday() {
  uint8_t day = now.day();
  uint8_t month = now.month();
  
  return
    (day == 15 && month == 2) || // whitaker
    (day == 26 && month == 3) || // hannah
    (day == 28 && month == 5) || // ross
    (day ==  3 && month == 7) || // robyn
    (day == 29 && month == 7) || // tyler
    (day ==  8 && month == 9)    // laurie
    ;
}
