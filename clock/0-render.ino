/***********************************************
 * render stuff
/*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/

void renderBlank(uint32_t diff, uint32_t duration) {
  if (!shouldRender(diff, DEFAULT_FS)) return;
  if (millis() - phaseEnteredMillis > duration) advance();
  
  off();
  render(DEFAULT_FS);
}

/** reflect the index map over y = -x, for setup() */
void reflectMap() {
  for (int x = 0; x < NUM_COLS; x++) {
    for (int y = 0; y < NUM_ROWS; y++) {
      INDEX_MAP[x][y] = _INDEX_MAP[NUM_ROWS - 1 - y][x];
    }
  }
}


/** Display the minute (er, all the words related to the minute): */
void renderMinute(uint8_t displayMinute) {
  if (displayMinute <=  0 + AROUND_OFFSET); // noop case to avoid the others 
  else if (displayMinute <=  5 + AROUND_OFFSET) {
    minuteFive();
    minutes();
  } else if (displayMinute <= 10 + AROUND_OFFSET) {
    minuteTen();
    minutes();
  } else if (displayMinute <= 15 + AROUND_OFFSET) {
    minuteQuarter();
  } else if (displayMinute <= 20 + AROUND_OFFSET) {
    minuteTwenty();
    minutes();
  } else if (displayMinute <= 25 + AROUND_OFFSET) {
    minuteTwenty();
    minuteFive();
    minutes();
  } else if (displayMinute <= 30 + AROUND_OFFSET) {
    minuteHalf();
  }
}

/** Display the hour for the given hour */
void renderHour(uint8_t displayHour) {
  // Now we know which hour (0-23) we're gonna show.
  // Hours 0-11 are am, 12-23 are pm:
  if (displayHour < 12) {
    am();
  } else {
    pm();
  }
  // figure out which hour number to display:
  displayHour %= 12;

  switch (displayHour) {
    case 0: hourTwelve(); break;
    case 1: hourOne(); break;
    case 2: hourTwo(); break;
    case 3: hourThree(); break;
    case 4: hourFour(); break;
    case 5: hourFive(); break;
    case 6: hourSix(); break;
    case 7: hourSeven(); break;
    case 8: hourEight(); break;
    case 9: hourNine(); break;
    case 10: hourTen(); break;
    case 11: hourEleven(); break;
  }
}

/** wireframe, 0 is outermost */
void renderSquare(int sq) {
  int hue = _sqHue(sq);
  uint32_t color = strip.gamma32(strip.ColorHSV(hue));
  
  /** top and bottom rows */
  for (int x = sq; x < NUM_COLS - sq; x++) {
    pixels[x][sq] = color;
    pixels[x][MAX_ROW - sq] = color;
  }

  /** sides */
  for (int y = sq + 1; y < NUM_ROWS - sq; y++) {
    pixels[sq][y] = color;
    pixels[MAX_COL - sq][y] = color;
  }
}
