/***********************************************
 * minute words:
/*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/

void itIs() {
  pixels[0][11] = WHITE;
  pixels[1][11] = WHITE;
  pixels[3][11] = WHITE;
  pixels[4][11] = WHITE;
}

void around() {
  for (int x = 6; x <= 11; x++) pixels[x][11] = WHITE;
}

void minuteFive() {
  for (int x = 7; x <= 10; x++) pixels[x][10] = WHITE;
}

void minuteTen() {
  for (int x = 9; x <= 11; x++) pixels[x][9] = WHITE;
}

void minuteQuarter() {
  pixels[0][8] = WHITE;
  for (int x = 2; x <= 8; x++) pixels[x][8] = WHITE;
}

void minuteTwenty() {
  for (int x = 0; x <= 5; x++) pixels[x][10] = WHITE;
}

void minuteHalf() {
  for (int x = 0; x <= 3; x++) pixels[x][9] = WHITE;
}

void minutes() {
  for (int x = 3; x <= 9; x++) pixels[x][7] = WHITE;
}

void past() {
  for (int x = 0; x <= 3; x++) pixels[x][6] = WHITE;
}

void til() {
  for (int x = 5; x <= 7; x++) pixels[x][6] = WHITE;
}
