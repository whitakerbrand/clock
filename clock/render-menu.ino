/***********************************************
 * rendering the menu 
/*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/

void menuLoop(uint32_t diff) {
  if (!shouldRender(diff, DEFAULT_FS)) return;
  
  off();
  switch (buttonMode) {
    case 0: _renderMinuteMenu(); break;
    case 1: renderHour(now.hour()); break;
    case 2: _renderDayMenu(); break;
    case 3: _renderMonthMenu(); break;
    case 4: _renderYearMenu(); break;
    case 5: colorAll(PURP); break;
  }
  render(DEFAULT_FS);
}

/** determines whether to display the button menu */
bool menuInProgress() {
  return buttonMode != -1;
}

/** Menu display functions: */
void _renderMinuteMenu() {
  uint8_t displayMinute = now.minute();

  if (now.minute() % 5 != 0) {
    around();
  }
  
  if (now.minute() > HALF_AN_HOUR + AROUND_OFFSET) {
    displayMinute = MINUTES_PER_HOUR - now.minute();
    if (now.minute() < MINUTES_PER_HOUR - AROUND_OFFSET) til();
  }
  // otherwise, the normal "past" case, if we aren't "around" the hour
  else if (now.minute() > AROUND_OFFSET) {
    past();
  }

  renderMinute(displayMinute);
  for (int i = 0; i < now.minute(); i++) {
    pixels[i / 5][i % 5] = WHITE;
  }
}

void _renderDayMenu() {
  for (int i = 0; i < now.day(); i++) {
    pixels[1 + i % 10][11 - (i / 10)] = WHITE;
  }
}

void _renderMonthMenu() {
  switch (now.month()) {
    case 1:  january(); break;
    case 2:  february(); break;
    case 3:  march(); break;
    case 4:  april(); break;
    case 5:  may(); break;
    case 6:  june(); break;
    case 7:  july(); break;
    case 8:  august(); break;
    case 9:  september(); break;
    case 10: october(); break;
    case 11: november(); break;
    case 12: december(); break;
  }
}

void _renderYearMenu() {
  uint16_t year = now.year();
  for (int place = 3; place >= 0; place--) {
    int digit = year % 10;
    for (int x = 0; x < 3; x++) {
      for (int y = 0; y < digit; y++) {
        pixels[x + 3 * place][y] = WHITE;
      }
    }
    year = year / 10;
  }
}
