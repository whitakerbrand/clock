/***********************************************
 * rendering birthdays
/*^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^*/

#define BIRTHDAY_MSG_SECONDS 2

void birthdayLoop(uint32_t diff) {
  if (!shouldRender(diff, DEFAULT_FS)) return;
  
  uint8_t day = now.day();
  uint8_t month = now.month();

  off();
  if (now.second() % 10 <= BIRTHDAY_MSG_SECONDS) happyBirthday(WHITE);
  else {
         if (day == 15 && month == 2)      whitaker(TEAL);
    else if (day == 26 && month == 3)      hannah(PURP);
    else if (day == 28 && month == 5)      ross(PURP);
    else if (day ==  3 && month == 7)      robyn(PURP);
    else if (day == 29 && month == 7)      tyler(PURP);
    else if (day ==  8 && month == 9)      laurie(RED);
    exclam(WHITE);      
  }
  render(DEFAULT_FS);  
}

/** when we should be showing birthday content */
bool birthdayInProgress() {
  return isBirthday() && now.second() % 10 <= BIRTHDAY_MSG_SECONDS * 2;
}
